{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "130b1fc6-92c2-4afc-8897-efb39fe67e09",
   "metadata": {},
   "source": [
    "# Jet Properties\n",
    "In this notebook, I outline the jet properties that will be used in the other notebooks, which explore different models for why e.g. the increase in radio emission lags behind the increase in soft X-rays.\n",
    "Note that *all* of these are estimates, so don't take them too seriously."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "efbbc772-e3ff-49fb-b506-304810a582a1",
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.optimize import fsolve\n",
    "import numpy as np\n",
    "from constants import Consts\n",
    "from scipy import special\n",
    "from scipy.optimize import brentq\n",
    "quiet = True"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "543c68fc-038d-4367-9a6e-519857fdc250",
   "metadata": {},
   "outputs": [],
   "source": [
    "pc_in_cm = 3.e18\n",
    "BH_mass = 1.e6\n",
    "BH_mass_cgs = BH_mass * Consts.Msun_cgs\n",
    "rg_in_cm = Consts.G_cgs * BH_mass_cgs / Consts.c_cgs**2\n",
    "rg_in_pc = rg_in_cm / pc_in_cm\n",
    "LBol = 1.e43\n",
    "lag_days = 200.\n",
    "lag_in_s = lag_days * 24 * 3600\n",
    "beta_jet = 0.2\n",
    "zEH_rg = 1.\n",
    "distance_in_pc = 80.e6\n",
    "p0 = 2."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "845f95aa-48ef-4bb7-866c-11511cbf81fe",
   "metadata": {},
   "source": [
    "## Jet Magnetic Field\n",
    "From Beloborodov 2017 Eq. 9, the magnetic field required to power the corona is $B=10^8(M/10M_\\odot)^{-1/2}~{\\rm G}$, which for our BH mass (assumed to be $10^6M_\\odot$) gives $B\\sim3\\times10^5$ G.\n",
    "Taking this value as the value at the black hole event horizon and assuming that the jet magnetic field decays as $1/z$ from the black hole, we have\n",
    "$$B(z)=B_0\\left(\\frac{z}{1r_g}\\right)^{-1}.$$\n",
    "This gives a value of 1 G at a distance of $3\\times10^5r_g$, i.e. 0.02 pc from the black hole."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "cdf2395b-d821-4807-b751-00290c1f3dfa",
   "metadata": {},
   "outputs": [],
   "source": [
    "def EH_Bfield(BH_mass_Msun):\n",
    "    \"Beloborodov 2017 Eq. 9\"\n",
    "    return 1.e8 / np.sqrt(BH_mass_Msun / 10.)\n",
    "\n",
    "\n",
    "B_EH = EH_Bfield(BH_mass)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "858b645f-3682-4c35-a14b-46d2d7ea2be1",
   "metadata": {},
   "outputs": [],
   "source": [
    "def jet_B_at_zrg(z_rg):\n",
    "    \"Assuming B is at 1rg (EH), decay as 1/z\"\n",
    "    return B_EH * (z_rg / zEH_rg)**(-1.)\n",
    "\n",
    "\n",
    "def jet_zrg_with_B(B_at_z):\n",
    "    \"Assuming B0 is at 1rg (EH), how far away do we jet B_at_z in rg?\"\n",
    "    return B_EH * zEH_rg / B_at_z\n",
    "\n",
    "\n",
    "z_1G = jet_zrg_with_B(1.)\n",
    "if not quiet:\n",
    "    print(\"We will have B=1G at z={:.2e}rg = {:.2e} pc from the BH\".format(\n",
    "        z_1G, z_1G * rg_in_cm / pc_in_cm))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ea94f119-7773-4259-923b-7086c345b1b8",
   "metadata": {},
   "source": [
    "## Jet Width\n",
    "How wide is the jet at a given distance from the black hole?\n",
    "Here we assume that $R_j(z)\\sim z^{1/2}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "cd781ad8-dbe9-4156-a809-35b5ad7050d3",
   "metadata": {},
   "outputs": [],
   "source": [
    "z0_rg = 10.\n",
    "width0_rg = z0_rg\n",
    "\n",
    "\n",
    "def jet_widthrg_at_zrg(z_rg):\n",
    "    \"Starting with jet width0 at z0=10 rg, assuming width ~ z^0.5\"\n",
    "    return width0_rg * (z_rg / z0_rg)**0.5"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63e3ee18-a3f2-48cd-bfcb-d3f29965bc30",
   "metadata": {},
   "source": [
    "## Jet Number Density\n",
    "What's the number density in the jet at a given distance from the black hole, assuming that $n_j(z)\\sim z^{-2}$?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "0dab6738-26cd-44e7-a503-fc295cef39e5",
   "metadata": {},
   "outputs": [],
   "source": [
    "Rc_rg = 10.\n",
    "n0_at_Rc_rg = 1. / (Consts.sigmaT_cgs * Rc_rg * rg_in_cm)\n",
    "\n",
    "\n",
    "def jet_ndensity_at_zrg(z_rg):\n",
    "    \"Assume n0 at z0, n~1/z^2, what is n at z rg?\"\n",
    "    return n0_at_Rc_rg * (z_rg / Rc_rg)**(-2.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "30a026a6-5929-47c2-bf2e-2501f9e82eec",
   "metadata": {},
   "source": [
    "## Jet Propagation Time\n",
    "Assuming a given, constant jet speed, how long does it take to propagate to a given distance?\n",
    "If we take $v_j=0.2c$, then:\n",
    "\n",
    "$$ t_{\\rm prop}=600\\frac{d}{0.1~{\\rm pc}}\\left(\\frac{v_j}{0.2c}\\right)^{-1}~{\\rm days}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "41c7c3f4-e6ac-4632-84e4-3ae2cea50629",
   "metadata": {},
   "outputs": [],
   "source": [
    "def jet_propagation_time(vjet, distance_cm):\n",
    "    return distance_cm / vjet\n",
    "\n",
    "\n",
    "dist_pc = 0.1\n",
    "dist_cm = dist_pc * pc_in_cm\n",
    "vjet = beta_jet * Consts.c_cgs\n",
    "ptime_s = jet_propagation_time(vjet, dist_cm)\n",
    "ptime_days = ptime_s / 24. / 3600.\n",
    "if not quiet:\n",
    "    print(\"For vjet={:.2f}c, it takes {:.2f} days to propagate to {:.1e}rg={:.1e}pc\".format(\n",
    "        beta_jet, ptime_days, dist_cm / rg_in_cm, dist_pc))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5d396d15-a9a9-438e-a751-10739553fc39",
   "metadata": {},
   "source": [
    "## Synchrotron Self-Absorption\n",
    "Is the jet optically thick or thin to synchrotron?\n",
    "In particular, do they emit thermally or as a power-law?\n",
    "This question is important for e.g. brightness temperature constraints."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "480adb02-ec37-4b04-a7dc-2a72791a61ff",
   "metadata": {},
   "source": [
    "### Jet SSA Frequency\n",
    "In the following, we assume that we're looking essentially perpendicular to the jet axis, so we look through the width of the jet.\n",
    "This also assumes that the magnetic field is constant through the width of the jet.\n",
    "For electrons with $\\gamma=2$ that emit quite close to the black hole, these assumptions give a self-absorption turnover frequency of 0.2 GHz.\n",
    "For electrons with $\\gamma=100$ that emit outside the absorbing shield, these assumptions give a self-absorption turnover frequency of $2\\times10^4$ Hz.\n",
    "Outside the shield, the lack of self-absorption is relatively robust to path length $L$, since for $p=2$,\n",
    "$$ \\nu_t\\sim n^{1/3}L^{1/3}B^{2/3}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "4848f918-bae5-4634-be22-ea9961e0a8d5",
   "metadata": {},
   "outputs": [],
   "source": [
    "%run radiative_tools.ipynb\n",
    "\n",
    "def jet_nut_at_zrg(z_rg, p0):\n",
    "    Rjz_cgs = jet_widthrg_at_zrg(z_rg) * rg_in_cm\n",
    "    nz_cgs = jet_ndensity_at_zrg(z_rg)\n",
    "    Bz_cgs = jet_B_at_zrg(z_rg)\n",
    "    path_length = 2. * Rjz_cgs\n",
    "    return solve_for_nut(path_length, p0, nz_cgs, Bz_cgs)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "974aa23e-709e-4b7d-a80d-109338b7dede",
   "metadata": {},
   "source": [
    "### Characteristic Height of 5 GHz\n",
    "At what height does the emission become optically-thin to synchrotron at a given frequency?\n",
    "With $1=2R_j(z_c)\\alpha_{\\nu=5~{\\rm GHz}}(z_c)$, solve for $z_c$.\n",
    "$$\\frac{z_c}{z_0}=\\left(2R_{j0}\\alpha_{\\nu_c0}\\right)^{2/(p+5)}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "71b07026-db68-4167-9933-c1f4e393c532",
   "metadata": {},
   "outputs": [],
   "source": [
    "def match_nuc_nut(z_rg, nuc, p0):\n",
    "    nut = jet_nut_at_zrg(z_rg, p0)\n",
    "    return nuc - nut\n",
    "\n",
    "\n",
    "def jet_zc_at_nuc(nuc, p0):\n",
    "    zmin = 1.\n",
    "    zmax = 1.e8\n",
    "    # print(match_nuc_nut(zmin, nuc, p0))\n",
    "    # print(match_nuc_nut(zmax, nuc, p0))\n",
    "    args = (nuc, p0)\n",
    "    zc_rg = brentq(match_nuc_nut, zmin, zmax, args=args)\n",
    "    return zc_rg"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
